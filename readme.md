PoC ADR use case 2
==============================================

Processo di Accertamento costi/investimento
----------------------------------------------

![BPMN](src/main/resources/com/adr/accerta/accerta.accertamento-proc-svg.svg)

Scelte di implementazione
----------------------------------------------

- Fase 6: approvazione congiunta è stata implementata come una sequenza approvazione RGC seguito da approvazione e attuazione modifica di ROC: è inutile chiedere l'intervento di ROC per approvare e poi dopo per modificare, conviene unificare.
- Fase 7: sebbene logicamente avviene dopo i passi precedenti, non è connessa con essi, poiché è una operazione ciclica che puè riguardare anche più caricamenti. Quindi, non è connessa con la singola instanza di processo
- Fase 8: come per la 7 non è connessa alla singola instanza di processo.

**Nota:** le fasi 7 e 8 richiedono uno *scheduler*, in mancanza d'altro è possibile utilizzare il PAM allo scopo, es.: processo avviato da un timer (con un solo task?).